<?php
include_once("./_func.php");

$homepage = pages('/');
$settings = pages('/settings/');

$browserTitle = page('browser_title_OR_headline_OR_title') . ' - ' . $settings->headline;
$title = page('headline_OR_title');
$pageTitle = (page('id') == 1) ? "" :"<h1 class='content-title'>$title</h1>";
$content = page()->render->body;

$sidebar =  $wb->subNavWidget2();  //(page('sidebar')) ? page('sidebar') : '';

$headScript ='';
$footScript ='';



