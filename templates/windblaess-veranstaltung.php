<?php
// event_date
// event_custom_date
// event_place
// body
// images
// files
$concertDate = "<p class='date'>";
$concertDate .= (page()->event_custom_date) ? page()->event_custom_date : date('j.n.Y, H:i', page()->event_date) . "Uhr";
$concertDate .= "</p>";
$eventPlace = "<p class='location'>" . page()->event_place . "</p>";

$download = $wb->downloads(page()->files, "Flyer als pdf");

$content = $wb->prepend($concertDate.$eventPlace, $content.$download);
