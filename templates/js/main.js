
//@codekit-prepend "./vendors/jquery.mobilemenu.js"

function iframe() {
  $('iframe').each(function(){
    var width = $(this).width();
    var height = Math.ceil(width / 1.7777);
    $(this).css('height', height + "px");
  });
}
function spendenBetrag() {
  if ( $('#Inputfield_spende_uber_den_betrag_von').is(':checked') ) {
  	$('.Inputfield_spendenbetrag').show();
  } else {
  	$('.Inputfield_spendenbetrag').hide();
  }
}
$(document).ready(function() {
	$('<div class="mobile-header"><span class="menu-toggle" >&#9776;</span></div><div class="mm-container"></div>').prependTo('body');

	$('.mm-container').mobileMenu({mobileElements: '.top-nav'});

	var $menuToggle = $('.menu-toggle');

	$('.mm-container').hide();
	$menuToggle.addClass('closed');
	$menuToggle.click(function(){
		$('.mm-container').fadeToggle();
		if ($(this).hasClass('closed')) {
			$(this).html('&times;').removeClass('closed');
		} else {
			$(this).html('&#9776;').addClass('closed');
		}

	});

  var subLevelActive = $('.topnav .subnav li.active a').length ? $('.topnav .subnav li.active a') : null;
  if (subLevelActive !== null){
    $('.sub-nav.widget li li a').each(function() {
      var href = $(this).attr('href');
      if (href === subLevelActive.attr('href')) {
        $(this).parent().addClass('current');
      }
    });
  }



  iframe();

  $(window).resize(function() {
    iframe();
  });
  
  spendenBetrag();
  $('#Inputfield_spende_uber_den_betrag_von').change(function(){
	  spendenBetrag();
  });

});
