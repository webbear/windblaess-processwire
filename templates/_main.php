<!DOCTYPE html>
<html class="<?=$wb->cssClasses() ?> no-js">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    <title><?= $browserTitle; ?></title>

    <?php if(page('summary')):?>
        <meta name="description" content="<?= $wb->tagStripper(page('summary')); ?>" />
    <?php endif; ?>
    <link rel="stylesheet" href="<?=$wb->makeAssetLink(urls('templates').'bower_components/font-awesome/css/font-awesome.min.css') ?>" type="text/css" media="screen" />

    <link rel="stylesheet" type="text/css" href="<?=$wb->makeAssetLink(urls('templates').'css/main.css') ?>" media="all" />
    <script src="<?=$wb->makeAssetLink(urls('templates').'js/vendors/modernizr.custom.js') ?>"></script>
    <script src="<?=$wb->makeAssetLink(urls('templates').'js/libs/jquery-3.1.1.min.js') ?>"></script>
     <script src="<?=$wb->makeAssetLink(urls('templates').'js/libs/jquery-migrate-3.0.0.min.js') ?>"></script>
    <?=$headScript?>
    <link rel="icon" type="image/x-icon" href="<?=urls('templates')?>images/favicon.ico" />
</head>
<body id="top" class="<?php if($sidebar) echo "has-sidebar"; ?>">
    <div class="container">

    <header class="header">


        <?php if(count($settings->images)): ?>
            <?=$wb->image($settings->images->first()->size(1200,0));?>
        <?php endif; ?>
        <div class="top-bar">
            <nav class="top-nav">
                <ul class='topnav'>
                    <?=$wb->renderNavigation($homepage->children, ["excluded_templates" => "windblaess-orgel|windblaess-veranstaltung|windblaess-presse|windblaess-archiv-eintrag"])?>
                </ul>
            </nav>
        </div>

    </header>

    <div class='body'>
        <h1 class="site-title"><a href="<?=$homepage->url?>"><?=$settings->headline?></a></h1>
        <div class='main-content'>
            <?=$pageTitle?>
            <?=$content?>
        </div>
        <?php if($sidebar): ?>
        <div class='sidebar'>
            <?=$sidebar?>
        </div>
        <?php endif; ?>
    </div>

    <footer class='footer'>
        <?=$wb->repeater($settings->repeater_widget, ['wrap_class' => 'footer-widgets']);?>

        <!--
        <ul class="additionals">
            <li class="copyright">&copy;<?=date('Y')?> <?=$settings->headline?></li>
            <li class="webbear">designed by <a href="http://www.webbear.ch">webbear.ch</a></li>
        </ul> -->

    </footer>
</div>
 <?=$wb->renderLogoutLink($user)?>

<?=$wb->isEditable()?>

<script src="<?=$wb->makeAssetLink(urls('templates') .'js/main.min.js')?>"></script>
<?=$footScript?>
</body>
</html>
