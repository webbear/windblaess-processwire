<?php

//images
//files
//body
// automatic children nav



if (page()->hasChildren) {
    $select = (count(page()->select_item)) ? page()->select_item : null;
    $ulClass = 'listing no-list-type';
    $liClass = '';
    $children = [];
    $count = 1;
    $out = '';

    // veranstaltungen
    if ($select == "1") {
        $thisYear = date('Y', time());
        $segments = $input->urlSegments();
        $years = $wb->getYearsList(page()->children("template=windblaess-veranstaltung"));
				$now  = date('Y-m-d');
				$nowYear = date('Y');
        if (count($segments) == 0) {
            $children = page()->children("template=windblaess-veranstaltung, event_date>=$now, sort=event_date"); //event_date>=$thisYear-01-01
        } else {
            $year = $input->urlSegment1;
            if (in_array($year, $years)) {
                $pageTitle = "<h1 class='content-title'>Archiv</h1>";
                $out .= "<h2 class='title-archives'>" . __("Veranstaltungen ") . $year . "</h2>";
								if ($year == $nowYear) {
									$end = $now;
								} else {
									$end = "$year-12-31";
								}
                $children = page()->children("template=windblaess-veranstaltung, event_date>=$year-01-01, event_date<=$end");
            } else {
                $out .= "<p class='no-events'>" . __("Leider wurden keine Veranstaltungen gefunden im Jahr ") . "{$year}</p>";
            }
        }
        $out .= $wb->renderEvents($children);
        $out .= "<div class='events-archives'><p>".__("Blättern Sie in unserem Archiv")."</p>" . $wb->buildArchives  ($years, page()->url) ."</div>";

    } else {
        if ($select == "3") {
            $ulClass .= ' organs';
            $liClass = 'organ organ';
            $children = page()->children("template=windblaess-orgel");

        } elseif ($select == "2") {
            $ulClass .= ' presse-listing';
            $liClass = 'presse presse';
            $children = page()->children("template=windblaess-presse");
        } elseif($select == "4") {
            $children = page()->children("template=windblaess-archiv-eintrag");
            $ulClass .= ' archives';
            $liClass = 'archiv-entry archiv-entry';
        }

        $out .= "<ul class='$ulClass'>";


        foreach ($children as $child) {
            $out .= "<li class='{$liClass}{$count}'>";
            $more = "<p class='more'><a href='{$child->url}'>".__("Alles lesen ...")."</a></p>";
            if ($select=="2") {
                $download = $wb->downloads($child->files);
                $out .= "<h2>{$child->title}</h2>";
                $out .= "<div class='abstract'>{$child->body}</div>";
                $out .= $download;
            }

            if ( $select == "3" ) {
                $out .= "<h2><a href='{$child->url}'>{$child->headline_OR_title}</a></h2>";
				$out .= ($child->collection) ? "<p class='collection'>{$child->collection}</p>" : "";
				$out .= (count($child->images)) ? "<div class='image'>" . $wb->image($child->images->first()->size(320,0)) . "</div>" : "";
                $out .= "<div class='abstract'>{$child->summary}</div>";
                
                $out .= $more;
            }

            if ($select == "4") {
                if ($child->body && $child->summary) {
                    $out .= "<h2><a href='{$child->url}'>{$child->headline_OR_title}</a></h2>";
                    $out .= "<div class='abstract'>{$child->summary}</div>";
                    $out .= $more;
                }
                if (!$child->summary) {
                    $out .= "<h2>{$child->headline_OR_title}</h2>";
                    $out .= "<div class='rte'>$child->body</div>";
                    $out .= $wb->downloads($child->files);
                }
            }
            $out .= "</li>";
            ++$count;
        }
        $out .= "</ul>";
    }

    if ($select == null) {
        $children = page()->children;
        $out .= "<ul class='$ulClass'>";
        foreach ($children as $child) {
            $out .="<li class='{$liClass}{$count}'><a href={$child->url}'>{$child->title}</a></li>";
            ++$count;
        }
        $out .= "</ul>";
    }

    $content .= $out;
}